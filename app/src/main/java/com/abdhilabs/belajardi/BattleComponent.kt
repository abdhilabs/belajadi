package com.abdhilabs.belajardi

import dagger.Component

@Component
interface BattleComponent {
    //digunakan untuk mengambil context dari activity yang akan menerima Inject.
    fun inject (context: MainActivity)
}