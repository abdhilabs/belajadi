package com.abdhilabs.belajardi

/**
 * Kelas ini akan diakses oleh DarkKnight dan LightKnight
 */
class Senjata(var namaTombak: String, var namaPedang: String) {

    //Membuat 2 buah fungsi
    fun tombak(): String {
        return namaTombak
    }

    fun pedang(): String {
        return namaPedang
    }
}