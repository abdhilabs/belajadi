package com.abdhilabs.belajardi

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    //Inject Variablenya
    @Inject lateinit var darkKnight: DarkKnight
    @Inject lateinit var lightKnight: LightKnight

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Mendapatkan Context dari class yang bersangkutan
        DaggerBattleComponent.create().inject(this)

        click_me.setOnClickListener {
            report.text = "${lightKnight.setEquip()} dan ${darkKnight.setEquip()}"
        }
    }
}
