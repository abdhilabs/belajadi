package com.abdhilabs.belajardi

import javax.inject.Inject

/**
 * Kenapa kita menambahkan Anotasi @Inject pada Constructor,
 * karena dengan begitu kita tidak harus membuat Instance / Objek dari class tersebut,
 * didalam MainActivity kita cukup membuat Variablenya saja.
 */

class DarkKnight @Inject constructor() {

    //Membuat fungsi yang mengembalikan nilai string
    fun setEquip(): String {
        return "Kelompok DarkKnight bertempur menggunakan Tombak"
    }
}